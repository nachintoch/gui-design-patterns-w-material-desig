package mx.nachintoch.materialdesign.wearable;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.preference.PreferenceManager;

/**
 * App's main screen, which shows a vertical stack.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 2.0, march 2019
 * @since Material Design Example App 1.0, march 2019
 */
public class MainActivity extends DrawerMenuActivity {

    // class attributes

    /**
     * Reference to the button which launchs the SecondActivity.
     * @since Main Activity 1.0, march 2019
     */
    @Nullable
    private Button launchInfiniteListButton;

    /**
     * Name of the shared View between this activity and the Second Activity; so this shared
     * element transforms in-screen during the Activity transition.
     * @since Main Activity 1.0, march 2019
     */
    @Nullable
    private String sharedViewTransitionName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initMenu(R.id.menu_drawer);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
    }//onCreate

    /**
     * Starts the infinite list activity.
     * @param openButton - Reference to the button which launch the infinite list activity.
     * @since Main Activity 1.0, march 2019
     */
    public void openInfiniteList(View openButton) {
        Intent starter = new Intent(this, SecondActivity.class);
        if(sharedPreferences.getBoolean(getString(R.string.enable_transitions_preference_key), true)) {
            if (launchInfiniteListButton == null)
                launchInfiniteListButton = findViewById(R.id.launch_infinite_list_button);
            if (sharedViewTransitionName == null)
                sharedViewTransitionName = getString(R.string.shared_button_transitionName);
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    this, launchInfiniteListButton, sharedViewTransitionName);
            startActivity(starter, options);
        } else startActivity(starter);
    }//openInfiniteList

}//MainActivity
