package mx.nachintoch.materialdesign.wearable;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import mx.nachintoch.materialdesign.commons.SettingsFragment;

/**
 * Activity which holds and displays a Preferences Fragment.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, march 2019
 * @since Material Design Example App 3.0, march 2019
 */
public class SettingsActivity extends DrawerMenuActivity
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, new SettingsFragment()).commit();
    }//onCreate

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }//onResume

    @Override
    protected void onPause() {
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }//onPause

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_settings) return true;
        return super.onOptionsItemSelected(item);
    }//onOptionsItemSelected

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(!themePreferenceKey.equals(key)) return;
        String selectedPreference = sharedPreferences.getString(key, darkThemeId);
        applyTheme(selectedPreference, true);
    }//onSharedPreferenceChanged

}//Settings Activity
