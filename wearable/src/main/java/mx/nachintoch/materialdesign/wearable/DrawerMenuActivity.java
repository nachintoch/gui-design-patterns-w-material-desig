package mx.nachintoch.materialdesign.wearable;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.PreferenceManager;
import androidx.wear.ambient.AmbientModeSupport;
import androidx.wear.widget.drawer.WearableActionDrawerView;
import android.util.Log;
import android.view.MenuItem;

import mx.nachintoch.materialdesign.commons.MenuHelper;
import mx.nachintoch.materialdesign.commons.ThemePreferenceAwareActivity;

/**
 * Basic Menu Activity which reacts to actions in the Menu Drawer.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.5, march 2019
 * @since GUI design patters w/Material design example 1.0, march 2019
 */
public abstract class DrawerMenuActivity extends FragmentActivity
        implements MenuItem.OnMenuItemClickListener,
        AmbientModeSupport.AmbientCallbackProvider, ThemePreferenceAwareActivity {

    // class attributes

    /**
     * Id of the default app's theme.
     * @since Main Menu Activity 2.0, march 2019
     */
    protected String darkThemeId;

    /**
     * Holds the Key for the <tt>SharedPreferences</tt> to store the theme preference.
     * @since Settings Activity 1.0, march 2019
     */
    protected String themePreferenceKey;

    /**
     * Reference to the SharedPreferences, as retrieved by each Activity.
     * @since Main Menu Activity 2.0, march 2019
     */
    protected SharedPreferences sharedPreferences;

    /**
     * Launcher for the new ActivityResult API, listens to results from other activities.
     * @since Main Menu Activity 3.0, february 2022
     */
    private final ActivityResultLauncher<Intent> resultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    int resultCode = result.getResultCode();
                    Intent data = result.getData();
                    if (resultCode == MenuHelper.RESULT_EXIT) {
                        setResult(MenuHelper.RESULT_EXIT);
                        finish();
                    } else if(resultCode == MenuHelper.RESULT_CHECK_STYLE && data != null) {
                        try {
                            int selectedTheme = getThemeResourceIdFromPreferenceId(
                                    data.getStringExtra(themePreferenceKey));
                            if (getPackageManager().getActivityInfo(getComponentName(), 0)
                                    .getThemeResource() == selectedTheme) return;
                            setTheme(selectedTheme);
                            recreate();
                        } catch (PackageManager.NameNotFoundException e) {
                            Log.w(DrawerMenuActivity.class.getSimpleName(), "Can't check current style", e);
                        }//tries to get current Activity theme
                    }
                }
            }
    );

    // methods

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        darkThemeId = getString(R.string.dark_theme_preference_id);
        themePreferenceKey = getString(R.string.theme_preference_key);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        applyTheme(sharedPreferences.getString(themePreferenceKey, darkThemeId), false);
    }//onCreate

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.menu_about) {
            MenuHelper.showAboutDialog(this);
            return true;
        }
        if (itemId == R.id.menu_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        if (itemId == R.id.menu_close_app) {
            setResult(MenuHelper.RESULT_EXIT);
            super.finish();
            return true;
        }
            return super.onOptionsItemSelected(item);
    }//onMenuItemClick

    @Override
    public AmbientModeSupport.AmbientCallback getAmbientCallback() {
        // ambient mode not supported
        return null;
    }//getAmbientCallback

    /**
     * Initialices the Drawer Menu
     *
     * @param id - The ID of the WearableActionDrawerView which contains the app's Menu.
     * @since Drawer Menu Activity 1.5, march 2019
     */
    protected final void initMenu(int id) {
        WearableActionDrawerView wearableActionDrawerView = findViewById(id);
        wearableActionDrawerView.getController().peekDrawer();
        wearableActionDrawerView.setOnMenuItemClickListener(this);
    }//initMenu

    @Override
    public void finish() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(themePreferenceKey,
                sharedPreferences.getString(themePreferenceKey, darkThemeId));
        setResult(MenuHelper.RESULT_CHECK_STYLE, resultIntent);
        super.finish();
    }//finish

    @Override
    public void startActivity(Intent intent) {
        resultLauncher.launch(intent);
    }//startActivity

    public void startActivity(Intent intent, ActivityOptionsCompat options) {
        resultLauncher.launch(intent, options);
    }//startActivity

    @Override
    public void applyTheme(String themeKey, boolean recreate) {
        setTheme(getThemeResourceIdFromPreferenceId(themeKey));
        if(recreate) recreate();
    }//applyTheme

    @Override
    public int getThemeResourceIdFromPreferenceId(String stylePreferenceId) {
        if(darkThemeId.equals(stylePreferenceId)) return R.style.DarkTheme;
        else return R.style.LightTheme;
    }//getThemeResourceIdFromPreferenceId

}//MenuListener
