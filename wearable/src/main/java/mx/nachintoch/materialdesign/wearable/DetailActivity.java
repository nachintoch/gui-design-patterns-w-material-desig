package mx.nachintoch.materialdesign.wearable;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.ColorInt;
import android.view.View;
import android.widget.TextView;

import mx.nachintoch.materialdesign.commons.DetailScreen;

/**
 * Wearable activity which is analogous to the Activity of the same name in the "phablet" module.<br/>
 * There is no implementation of the <tt>support.v4.app.Fragment</tt> class for Wearable Devices;
 * so we would have to rely on the original <tt>android.app.Fragment</tt> class; which is OK, but
 * we would have to write an identical Fragment class just for this module, so it's easier to just
 * use a single WearableActivity.<p/>
 * In any case, the Layout of this Activity includes the same <i>fragment layout</i> used in the
 * <b>Master Detail Flow</b> in the "phablet" module.<p/>
 * We use all Wearable devices with a separated Detail Activity from the List, since any way
 * Wearable devices are some of the smallest in the Android universe, so a complete implementation
 * of the <i>Master Detail Flow</i> pattern for Wearables would not make much sense.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, march 2019
 * @since Material Design Example App 2.0, march 2019
 */
public class DetailActivity extends DrawerMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        TextView selectedItemTitle = findViewById(R.id.entry_title_tv);
        View colorView  = findViewById(R.id.color_view);
        TextView red = findViewById(R.id.red_value_tv);
        TextView green = findViewById(R.id.green_value_tv);
        TextView blue = findViewById(R.id.blue_value_tv);
        TextView hex = findViewById(R.id.color_hex_value_tv);
        Intent startIntent = getIntent();
        selectedItemTitle.setText(startIntent.getStringExtra(DetailScreen.ENTRY_MESSAGE_KEY));
        float[] rgb = DetailScreen.generateColorFromIndex(
                startIntent.getIntExtra(DetailScreen.MASTER_LIST_SIZE_KEY, -1),
                startIntent.getIntExtra(DetailScreen.INDEX_KEY, -1));
        int[] intRgb = new int[] {(int) (rgb[0] *255), (int) (rgb[1] *255), (int) (rgb[2] *255)};
        @ColorInt int indexColor = Build.VERSION.SDK_INT < Build.VERSION_CODES.O ?
                Color.rgb(intRgb[0], intRgb[1], intRgb[2]) :
                Color.rgb(rgb[0], rgb[1], rgb[2]);
        colorView.setBackgroundColor(indexColor);
        red.setText(getResources().getString(R.string.red_color_component, rgb[0], intRgb[0]));
        green.setText(getString(R.string.green_color_component, rgb[1], intRgb[1]));
        blue.setText(getString(R.string.blue_color_component, rgb[2], intRgb[2]));
        hex.setText(getString(R.string.hex_color, Integer.toHexString(indexColor)));
        initMenu(R.id.menu_drawer);
    }//onCreate

}//Detail DrawerMenu Activity
