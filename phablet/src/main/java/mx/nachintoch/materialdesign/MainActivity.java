package mx.nachintoch.materialdesign;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.preference.PreferenceManager;

/**
 * Launcher activity. This is the first Activiity the user will interact when launching the app.
 * @since <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, march 2019
 * @since GUI design patters w/Material design example 1.0, march 2019
 */
public class MainActivity extends MainMenuActivity {

    // class attributes

    /**
     * Reference to the button which launchs the SecondActivity. It is not used if running on API
     * level lower than 21.
     * @since Main Activity 1.0, march 2019
     */
    @Nullable
    private Button launchInfiniteListButton;

    /**
     * Name of the shared View between this activity and the Second Activity; so this shared
     * element transforms in-screen during the Activity transition.
     * @since Main Activity 1.0, march 2019
     */
    @Nullable
    private String sharedViewTransitionName;

    // methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vertical_list);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
    }//onCreate

    /**
     * Starts the infinite list activity.
     * @param openButton - Reference to the launchInfiniteListButton which launch the infinite list activity.
     * @since Main Activity 1.0, march 2019
     */
    @SuppressWarnings("unused")
    public void openInfiniteList(View openButton) {
        Intent starter = new Intent(this, SecondActivity.class);
       if(!sharedPreferences.getBoolean(getString(R.string.enable_transitions_preference_key), false)
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            startActivity(starter);
        else {
            if(launchInfiniteListButton == null)
                launchInfiniteListButton = findViewById(R.id.launch_infinite_list_button);
            if(sharedViewTransitionName == null)
                sharedViewTransitionName = getString(R.string.shared_button_transitionName);
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    this, launchInfiniteListButton, sharedViewTransitionName);
            startActivity(starter, options);
        }//if running Lollipop or higher
    }//openInfiniteList

}//MainActicity
