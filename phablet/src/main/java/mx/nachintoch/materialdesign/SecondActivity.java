package mx.nachintoch.materialdesign;

import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.nachintoch.materialdesign.commons.DetailScreen;
import mx.nachintoch.materialdesign.commons.InfiniteListAdapter;
import mx.nachintoch.materialdesign.commons.MasterListLoader;
import mx.nachintoch.materialdesign.provider.InfiniteListDBContract;
import mx.nachintoch.materialdesign.provider.ListDbHelper;

/**
 * View which holds a button that when pressed appends a element to the Recicler View bellow the
 * button.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 3.0, april 2019
 * @since GUI design patters w/Material design example 1.0, march 2019
 */
public class SecondActivity extends MainMenuActivity
        implements InfiniteListAdapter.MasterListItemClickHandler,
        LoaderManager.LoaderCallbacks {

    // class attributes

    /**
     * An adapter to "push" data into the list.
     * @since Second Activity 1.0, march 2019
     */
    private InfiniteListAdapter listAdapter;

    /**
     * Remembers if the Details Panel is available in the layout. This will only happen if the
     * device in which the app is running has a large screen (sw-600dp).
     * @since Second Activity 2.0, march 2019
     */
    private boolean isDetailsPaneAvailable;

    /**
     * Reference to the progress bar over the Infinite List. This will be hided after the List has
     * been loaded.
     * @since Second Activity 2.0 march 2019
     */
    private ProgressBar progressBar;

    /**
     * Remembers if the list size has changed.
     * @since Second Activity 3.0, april 2019
     */
    private boolean hasListChanged;

    // methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_second);
        Button button = findViewById(R.id.add_to_list_button);
        button.setOnClickListener(this::addListElement);
        RecyclerView infiniteList = findViewById(R.id.infinite_list);
        progressBar = findViewById(R.id.infinite_list_progress_bar);
        infiniteList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        infiniteList.setLayoutManager(layoutManager);
        isDetailsPaneAvailable = findViewById(R.id.color_detail_holder) != null;
        listAdapter = new InfiniteListAdapter(getResources(), this);
        infiniteList.setAdapter(listAdapter);
        LoaderManager.getInstance(this).initLoader(ListDbHelper.LIST_SIZE_LOADER_ID,
                null, this);
    }//onCreate

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus && isDetailsPaneAvailable)
            setRequestedOrientation(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2 ?
                    ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE :
                    ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
    }//onWindowFocusChanged

    @Override
    protected void onStop() {
        if(hasListChanged) {
            ContentValues values = new ContentValues();
            values.put(InfiniteListDBContract.InfiniteListEntry.COLUMN_LIST_SIZE,
                    listAdapter.getItemCount());
            getContentResolver().update(MasterListLoader.buildListSizeEntryUri(), values,
                    null, null);
            hasListChanged = false;
        }//if the list has changed
        super.onStop();
    }//onStop

    /**
     * Adds an element to the infinite list.
     * @param addButton - Reference to the Add Button, it is not used.
     * @since Second Activity 1.0, march 2019
     */
    public void addListElement(@Nullable View addButton) {
        listAdapter.addItem();
        hasListChanged = true;
    }//addListElement

    @Override
    public void onItemClicked(int clickedItemIndex, String entryText, int masterListSize) {
        if(isDetailsPaneAvailable) {
            Bundle detailFragmentArgs = new Bundle();
            detailFragmentArgs.putInt(DetailScreen.INDEX_KEY, clickedItemIndex);
            detailFragmentArgs.putString(DetailScreen.ENTRY_MESSAGE_KEY, entryText);
            detailFragmentArgs.putInt(DetailScreen.MASTER_LIST_SIZE_KEY, masterListSize);
            DetailFragment detailFragment = new DetailFragment();
            detailFragment.setArguments(detailFragmentArgs);
            getSupportFragmentManager().beginTransaction().replace(R.id.color_detail_holder,
                    detailFragment).commit();
        } else {
            Intent intent = new Intent(this, DetailActivity.class);
            DetailScreen.initDetailScreenStartIntent(intent, clickedItemIndex, masterListSize, entryText);
            startActivity(intent);
        }//adds the detail fragment if the screen is big enough or starts the detail Activity.
    }//onItemClicked

    @NonNull
    @Override
    public Loader onCreateLoader(int i, @Nullable Bundle bundle) {
        switch (i) {
            case ListDbHelper.LIST_SIZE_LOADER_ID :
                return new CursorLoader(this, MasterListLoader.buildListSizeEntryUri(),
                        new String[] {InfiniteListDBContract.InfiniteListEntry.COLUMN_LIST_SIZE},
                        null, null, null);
            case MasterListLoader.MASTER_LIST_LOADER_ID :
                boolean isDbInit = bundle != null;
                int listSize = isDbInit ? bundle.getInt(DetailScreen.MASTER_LIST_SIZE_KEY) : 1;
                return new MasterListLoader(this, listSize, isDbInit);
            default :
                throw new UnsupportedOperationException("Can't create a Loader with ID " + i);
        }//creates the appropriate loader
    }//onCreateLoader

    @Override
    public void onLoadFinished(@NonNull Loader loader, Object results) {
        if(loader.getId() == ListDbHelper.LIST_SIZE_LOADER_ID) {
            Cursor result = (Cursor) results;
            Bundle listLoaderParams;
            if(result.moveToFirst()) {
                listLoaderParams = new Bundle();
                listLoaderParams.putInt(DetailScreen.MASTER_LIST_SIZE_KEY,
                        result.getInt(0));
            } else {
                listLoaderParams = null;
            }
            LoaderManager.getInstance(this).initLoader(MasterListLoader.MASTER_LIST_LOADER_ID,
                    listLoaderParams, this).forceLoad();
        } else {
            listAdapter.setDataset((List<String>) results);
            progressBar.setVisibility(View.GONE);
        }//reacts to the finalization of a Loader
    }//onLoadFinished

    @Override
    public void onLoaderReset(@NonNull Loader loader) {
        // unused
    }//onLoaderReset

}//SecondActivity
