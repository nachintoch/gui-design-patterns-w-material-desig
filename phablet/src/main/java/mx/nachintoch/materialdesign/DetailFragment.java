package mx.nachintoch.materialdesign;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import mx.nachintoch.materialdesign.commons.DetailScreen;

/**
 * Fragment which holds the detail view of each list entry in the Infinite List, as part of the
 * <b>Master Detail Flow</b>.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, march 2019
 * @since Material Design Example App 2.0, march 2019
 */
public class DetailFragment extends Fragment {

    // attributes

    /**
     * Holds the index of the Master list's selected index of which detail must be shown.
     * @since Detail Fragment 1.0, march 2019
     */
    private int selectedIndex;

    /**
     * The size of the Master list. It's necessary as is the base to create a color spectrum to map
     * each of the Master list's indexes.
     * @since Detail Fragment 1.0, march 2019
     */
    private int masterListSize;

    /**
     * Holds the text of the Master list's selected entry of which detail must be shown.
     * @since Detail Fragment 1.0, march 2019
     */
    private String selectedEntryText;

    // methods

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args == null) {
            selectedIndex = -1;
            selectedEntryText = "";
            Log.w(DetailFragment.class.getSimpleName(), "Somehow an invalid entry was selected");
            return;
        }//if there are no args.
        selectedIndex = args.getInt(DetailScreen.INDEX_KEY);
        masterListSize = args.getInt(DetailScreen.MASTER_LIST_SIZE_KEY);
        selectedEntryText = args.getString(DetailScreen.ENTRY_MESSAGE_KEY);
    }//onCreate

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // root layout inflation
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        // get references from graphical components to change its default state
        TextView selectedItemTitle = rootView.findViewById(R.id.entry_title_tv);
        View colorView  = rootView.findViewById(R.id.color_view);
        TextView red = rootView.findViewById(R.id.red_value_tv);
        TextView green = rootView.findViewById(R.id.green_value_tv);
        TextView blue = rootView.findViewById(R.id.blue_value_tv);
        TextView hex = rootView.findViewById(R.id.color_hex_value_tv);
        // then we set all values
        selectedItemTitle.setText(selectedEntryText);
        float[] rgb = DetailScreen.generateColorFromIndex(masterListSize, selectedIndex);
        /*
         * In Android versions prior to Oreo (Android 8, API level 26), there was no direct way to
         * Create a color from Float RGB values; only using Integer RGB values in the range [0,255],
         * so if we are running in an older version of Android, we project the rgb variables in the
         * real range [0, 1] to the integer range [0, 255]
         */
        int[] intRgb = new int[] {(int) (rgb[0] *255), (int) (rgb[1] *255), (int) (rgb[2] *255)};
        @ColorInt int indexColor = Build.VERSION.SDK_INT < Build.VERSION_CODES.O ?
                Color.rgb(intRgb[0], intRgb[1], intRgb[2]) :
                Color.rgb(rgb[0], rgb[1], rgb[2]);
        colorView.setBackgroundColor(indexColor);
        red.setText(rootView.getResources().getString(R.string.red_color_component, rgb[0], intRgb[0]));
        green.setText(rootView.getResources().getString(R.string.green_color_component, rgb[1], intRgb[1]));
        blue.setText(rootView.getResources().getString(R.string.blue_color_component, rgb[2], intRgb[2]));
        hex.setText(getString(R.string.hex_color, Integer.toHexString(indexColor)));
        return rootView;
    }//onCreate

}//Detail Fragment
