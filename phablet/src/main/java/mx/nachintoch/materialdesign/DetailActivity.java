package mx.nachintoch.materialdesign;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;

import mx.nachintoch.materialdesign.commons.DetailScreen;

/**
 * Activity which only shows up in devices with screens which screen's smallest side has less than
 * 600dp. This Activity shows the detail of each element in the list, as part of the <b>Master
 * Detail Flow</b>.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, march 2019
 * @since Material Design Example App 2.0, march 2019
 */
public class DetailActivity extends MainMenuActivity {

    // methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent startIntent = getIntent();
        ActionBar actionBar = getSupportActionBar();
        String detailFragmentTitle = null;
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(startIntent.getStringExtra(DetailScreen.ENTRY_MESSAGE_KEY));
        } else {
            detailFragmentTitle = startIntent.getStringExtra(DetailScreen.ENTRY_MESSAGE_KEY);
        }
        Bundle detailFragmentArgs = new Bundle();
        detailFragmentArgs.putString(DetailScreen.ENTRY_MESSAGE_KEY, detailFragmentTitle);
        detailFragmentArgs.putInt(DetailScreen.INDEX_KEY,
                startIntent.getIntExtra(DetailScreen.INDEX_KEY, -1));
        detailFragmentArgs.putInt(DetailScreen.MASTER_LIST_SIZE_KEY,
                startIntent.getIntExtra(DetailScreen.MASTER_LIST_SIZE_KEY, -1));
        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(detailFragmentArgs);
        getSupportFragmentManager().beginTransaction().add(R.id.color_detail_holder, detailFragment)
                .commit();
    }//onCreate

}//Detail Activity
