package mx.nachintoch.materialdesign.commons;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Performs the ations for the menu items.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 2.0, march 2019
 * @since GUI design patters w/Material design example 1.0, march 2019
 */
public class MenuHelper {

    // class attributes

    /**
     * Activity result state which indicates that the application should shut down.
     * @since Menu Helper 1.0, march 2019
     */
    public static final byte RESULT_EXIT = 2;

    /**
     * Activity result state which indicates that the theme has been changed.
     * @since Menu Helper 2.0, march 2019
     */
    public static final byte RESULT_CHECK_STYLE = 3;

    // constructor

    /**
     * This private constructor prevents any other class to try to instance this.
     * @since Menu Helper 1.0, march 2019
     */
    private MenuHelper() {
        // does nothing at all
    }//constructor

    // static methods

    /**
     * Shows the about dialog
     * @param context - The context from where the dialog must be built.
     * @since Menu Helper 1.0, march 2019
     */
    public static void showAboutDialog(Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(R.string.menu_about)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setMessage(R.string.about)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }//onClick
                })
                .create().show();
    }//showAbutDialog

}//MenuHelper
