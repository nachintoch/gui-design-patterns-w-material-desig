package mx.nachintoch.materialdesign.commons;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import androidx.loader.content.AsyncTaskLoader;

import java.util.LinkedList;
import java.util.List;

import mx.nachintoch.materialdesign.provider.InfiniteListDBContract;

/**
 * Creates the list elements to insert on the Master Infinite List.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.1, march 2019
 * @since Material Design Example App, 2.0, march 2019
 */
public class MasterListLoader extends AsyncTaskLoader<List<String>> {

    // attributes

    /**
     * Holds the Mater List's contents, so they have not to be generated again.
     * @since MasterList Loader 1.0, march 2019
     */
    private List<String> listContents;

    /**
     * Remembers if the database has been initilized or not, to do eso if necessary.
     * @since Master List Loader 1.1, april 2019
     */
    private boolean isDatabaseInitialized;

    /**
     * Holds the previous list's size to recreate it.
     * @since MasterList Loader 1.0, march 2019
     */
    private final int LIST_SIZE;


    /**
     * ID for this Loader class, so only one instance will run at a time and is recognized by the
     * LoaderManager from other Loaders.
     * @since MasterList Loader 1.0, march 2019
     */
    public static final byte MASTER_LIST_LOADER_ID = 4;

    // constructor

    /**
     * Creates a MasterList Loader.
     * @param context - Application context.
     * @param listSize - Previous size of the Master List to recreate it.
     * @param isDbInit - Indicates if the database has been initialized or not.
     * @since Master List Loader 1.0, march 2019
     */
    public MasterListLoader(Context context, int listSize, boolean isDbInit) {
        super(context);
        LIST_SIZE = listSize;
        isDatabaseInitialized = isDbInit;
    }//constructor

    // methods

    @Override
    public List<String> loadInBackground() throws IllegalStateException {
        if(!isDatabaseInitialized) {
            ContentValues values = new ContentValues();
            values.put(InfiniteListDBContract.InfiniteListEntry.COLUMN_LIST_SIZE, 1);
            getContext().getContentResolver().insert(MasterListLoader.buildListSizeEntryUri(), values);
            isDatabaseInitialized = true;
        }//if the database is not initialized
        if(listContents != null) return listContents;
        listContents = new LinkedList<>();
        Resources res = getContext().getResources();
        for(int i = 0; i < LIST_SIZE; i++)
            listContents.add(i, InfiniteListAdapter.createMessageForEntry(i, res));
        return listContents;
    }//loadInBackground

    /**
     * Creates the URI that points to the only master list's size entry in the database.
     * @return The URI that points to the master list size.
     * @since Master List Loader 1.1, april 2019
     */
    public static Uri buildListSizeEntryUri() {
        return InfiniteListDBContract.InfiniteListEntry.buildListSizeUri(1);
    }//buildListSizeEntryUri

}//MasterListLoader
