package mx.nachintoch.materialdesign.commons;

import android.content.Intent;

/**
 * Helper class to make available for all projects the constants needed to ID the data to siplay
 * in the Master Detail Flow Detail Screen; either Activity or Fragment.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, march 2019
 * @since Material Design Example app 2.0, march 2019
 */
public final class DetailScreen {

    // attributes

    /**
     * Key to store and retrieve the Master list's selected index into or from a Bundle.
     * @since Detail Fragment 1.0, march 2019
     */
    public static final String INDEX_KEY = "mx.nachintoch.materialdesign.INDEX";
    /**
     * Key to store and retrieve the Master list's selected entry text into or from a Bundle.
     * @since Detail Fragment 1.0, march 2019
     */
    public static final String ENTRY_MESSAGE_KEY = "mx.nachintoch.materialdesign.ENTRY_MESSAGE";
    /**
     * Key to store and retrieve the Master list's size into or from a Bundle.
     * @since Detail Fragment 1.0, march 2019
     */
    public static final String MASTER_LIST_SIZE_KEY = "mx.nachintoch.materialdesign.MASTER_LIST_SIZE";

    // contructor

    /**
     * Hides the constructor so this class cannot be instantiated.
     * @since DetailScreen 1.0, march 2019
     */
    private DetailScreen() {
        // nope, nothing at all
    }//default constructor

    // static methods

    /**
     * Using the current Master List's size and the index selected by the user, this method
     * splits the Master List's length in three to generate a Color from its RGB components:
     * <ol>
     * <li>The first third of the Master List's length generates values for the Red component only;
     * Green and Blue are always zero.</li>
     * <li>The second third, generates values for the Red and Green components, while blue remains
     * as zero.</li>
     * <li>The last third, generates values for all RGB components.</li>
     * </ol><p/>
     * The values generated for the components are in the real range [0, 1]; represented with
     * floats in this implementation. The index where any third begins, produces values close to
     * zero (or zero) for the color components and the values grow directly proportional to 1 for
     * the last index of the list.<p/>
     * This method could be "better" and use even more combinations of the Master List subsegments
     * to create more combinations of RGB values growth; since this implementation with thirds won't
     * produce every possible color for any list, but is good enough for the example.
     * @return The color generated for the selected Master List's Item as a float array for the
     * values RGB (in that order)
     * @since Detail Fragment 1.0, march 2017
     */
    public static float[] generateColorFromIndex(int masterListSize, int selectedIndex) {
        float datasetThird = masterListSize /3f;
        byte third;
        int previousThird;
        if(masterListSize == 1 || selectedIndex < datasetThird) {
            previousThird = selectedIndex +1;
            third = 1;
        } else {
            previousThird = (int) datasetThird;
            third = (byte) (selectedIndex < datasetThird *2 ? 2 : 3);
        }//calculates which third is this and at what index the last third ends
        float[] rgb = new float[3];
        rgb[0] = (selectedIndex +1) /(third *datasetThird);
        rgb[1] = (selectedIndex +1 -previousThird) /(2 *datasetThird);
        rgb[2] = third == 3 ? (selectedIndex +1f) /masterListSize : 0;
        if(rgb[0] > 1) rgb[0] = 1;
        if(rgb[1] > 1) rgb[1] = 1;
        return rgb;
    }//generateColorFromIndex

    /**
     * Sets all the extra data in the intent used to Start a Detail Activity.
     * @param intent - The intent to start the Detail Activity.
     * @param selectedIndex - The Master List's selected item index.
     * @param masterListSize - The Master List's size.
     * @param selectedEntryText . The selected List entry text.
     */
    public static void initDetailScreenStartIntent(Intent intent, int selectedIndex, int masterListSize,
                                            String selectedEntryText) {
        intent.putExtra(DetailScreen.INDEX_KEY, selectedIndex);
        intent.putExtra(DetailScreen.ENTRY_MESSAGE_KEY, selectedEntryText);
        intent.putExtra(DetailScreen.MASTER_LIST_SIZE_KEY, masterListSize);
    }//initDetailScreenStartIntent

}//Detail Activity
