package mx.nachintoch.materialdesign.commons;

import android.os.Bundle;
import androidx.preference.PreferenceFragmentCompat;


/**
 * Settings fragment.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, march 2019
 * @since Material Design Example App 3.0, march 2019
 */
public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
    }//onCreatePreferences

}//Settings PreferenceFragment
