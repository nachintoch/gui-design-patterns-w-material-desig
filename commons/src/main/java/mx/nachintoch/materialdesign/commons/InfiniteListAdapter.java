package mx.nachintoch.materialdesign.commons;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

/**
 * List Adapter for the Infinite List.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 2.0, march 2019
 * @since GUI design patters w/Material design example 1.0, march 2019
 */
public class InfiniteListAdapter extends RecyclerView.Adapter<InfiniteListAdapter.ListEntry> {

    // attributes

    /**
     * List that holds the data to display in the list.
     * @since InfiniteList Adapter 1.0, march 2019
     */
    private List<String> dataset;

    /**
     * Holds the message to display in the list
     * @since InfiniteList Adapter 1.0, march 2019
     */
    private final Resources RESOURCES;

    /**
     * Reference to the List's Item Click Handler.
     * @since InfiniteList Adapter 2.0, march 2019
     */
    private final MasterListItemClickHandler CLICK_HANDLER;

    /**
     * Used to store and retrieve the size of the Infinite List into and from a Bundle.
     * @since InfiniteList Adapter 2.0, march 2019
     */
    public static final String INFINITE_LIST_SIZE_KEY =
            "mx.nachintoch.materialdesign.commons.INFINITE_LIST_SIZE";

    // constructor

    /**
     * Builds up a list adapter with the message to display in each entry.
     * @param res - The message to display.
     * @since InfiniteList Adapter 1.0, march 2019
     */
    public InfiniteListAdapter(Resources res, MasterListItemClickHandler listClickHandler) {
        RESOURCES = res;
        CLICK_HANDLER = listClickHandler;
    }//constructor

    // methods

    @NonNull
    @Override
    public ListEntry onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        TextView tv = (TextView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.infinite_list_entry, viewGroup, false);
        return new ListEntry(tv);
    }//onCreateViewHolder

    @Override
    public void onBindViewHolder(@NonNull ListEntry listEntry, int i) {
        listEntry.entryText.setText(dataset.get(i));
    }//onBindViewHolder

    @Override
    public int getItemCount() {
        return dataset != null ? dataset.size() : 0;
    }//getItemCount

    /**
     * Sets data to display in-screen.
     * @since IniniteList Adapter 1.0, march 2019
     */
    public void addItem() {
        if(dataset == null) dataset = new LinkedList<>();
        int i = dataset.size();
        dataset.add(i, createMessageForEntry(i, RESOURCES));
        notifyItemInserted(i);
    }//setDataset

    /**
     * Sets the list's contents to the given ones. Previous values will be lost,
     * @param dataset - A List of text to display atr the Master List.
     * @since Ifinite List Adapter 2.0, april 2019
     */
    public void setDataset(List<String> dataset) {
        this.dataset = dataset;
        notifyDataSetChanged();
    }//setDateset

    // static methods

    /**
     * Generates a message for a List's entry.
     * @param i - The index of the entry which content will be generated
     * @param res - Reference to the aplication's resources.
     * @return The message of the list's entry to create.
     */
    static String createMessageForEntry(int i, @NonNull Resources res) {
        return res.getString(R.string.infinite_list_entry_message,
                i +1);
    }//createMessageForEntry

    // nested classes

    /**
     * Defines the behaviour of an element in the Infinite List.
     * @author <a href="manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
     * @version 1.0, march 2019
     * @since GUI design patters w/Material design example 1.0, march 2019
     */
    class ListEntry extends RecyclerView.ViewHolder implements View.OnClickListener {

        // class attributtes

        /**
         * Reference to the contained TextView to set the text to a given one.
         * @since ListEntry 1.0, march 2019
         */
        private final TextView entryText;

        // constructors

        /**
         * Constructs a List Entry with the given text view to display.
         * @param entryTV - The textview to display in the list.
         * @since ListEntry 1.0, march 2019
         */
        ListEntry(TextView entryTV) {
            super(entryTV);
            entryText = entryTV;
            entryText.setClickable(true);
            entryText.setOnClickListener(this);
        }//constructor

        @Override
        public void onClick(View v) {
            CLICK_HANDLER.onItemClicked(getBindingAdapterPosition(), entryText.getText().toString(),
                    dataset.size());
        }//onClick

    }//ListEntry ViewHolder

    /**
     * Defines a handeler for click events on the Master Detail Flow's pattern List.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
     * @version 1.0, march 2019
     * @since InfiniteList Adapter 2.0, march 2019
     */
    public interface MasterListItemClickHandler {

        /**
         * Starts the Detail view to display data from the selected Master List's entry.
         * @param clickedItemIndex - The index of the selected Master List item.
         * @param entryText - The text displayed in the selected list's entry.
         * @param masterListSize - The total size of the Master List.
         * @since MasterListItem ClickHandler 1.0, march 2019
         */
        void onItemClicked(int clickedItemIndex, String entryText, int masterListSize);

    }//MasterListItem ClickHandler Interface

}//InifniteList Adapter
