package mx.nachintoch.materialdesign.commons;

import androidx.fragment.app.FragmentActivity;

/**
 * Interface that indicates how a Theme Preference Aware Activity on this application should
 * behave. When the Theme is changed in a
 * {@link androidx.preference.PreferenceFragmentCompat Preference Fragment}, it must be
 * propagated back to all app activities in the stack to show changes consistently to the user.<p/>
 * This general definition is not an Abstract Activity class since this indicates the behavior for
 * both "Phablet" and "Wearable" devices. The Phablet app's version uses an
 * {@link androidx.appcompat.app.ActionBar Action Bar}, so their Activities are subclasses of
 * {@link androidx.appcompat.app.AppCompatActivity AppCompatAcivity}; while Wearable devices screens
 * are too tiny for an Action Bar, so its not used and the app's Activities are subclasses of
 * {@link FragmentActivity FragmentActivity}.<p/>
 * Since multi-inheritance is not possible in Java, this interface approach is preferred.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, april 2019
 * @since Material Design Example App 3.1, april 2019
 */
public interface ThemePreferenceAwareActivity {

    /**
     * Sets the application's theme.
     * @param themeKey - The Id if the theme to use.
     * @param recreate - If the activity must be recreated so the changes has any effect
     * @since Main Menu Activity 2.0, march 2019
     */
    void applyTheme(String themeKey, boolean recreate);

    /**
     * Given a String which must be a valid Id for the app's themes, returns the ID of the style
     * resources for the given Id.
     * @param stylePreferenceId - The String Id of a app's theme.
     * @return The integer ID of the style resource for the given Id.
     * @since Main Menu Activity 2.0, march 2019
     */
    int getThemeResourceIdFromPreferenceId(String stylePreferenceId);

}//ThemePreferenceAwareActivity
