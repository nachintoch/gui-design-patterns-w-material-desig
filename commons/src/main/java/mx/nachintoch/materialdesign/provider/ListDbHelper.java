package mx.nachintoch.materialdesign.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Helper class to create and mantain the app's database.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, april 2019
 * @since Material Design Example app 3.0, april 2019
 */
public class ListDbHelper extends SQLiteOpenHelper {

    // attributes

    /**
     * Current version of the database.
     * @since ListDbHelper 1.0, april 2019
     */
    public static final byte DATABASE_VERSION = 1;

    /**
     * Name of the current version database's filename.
     * @since List DB Helper 1.0, april 2019
     */
    public static final String DATABASE_NAME = "material-design-example";

    /**
     * ID for a CursorLoader to interact with this database.
     * @since List DB Helper 1.0, april 2019
     */
    public static final byte LIST_SIZE_LOADER_ID = 3;

    // constructor

    /**
     * Creates a Database Helper.
     * @param context - The context which request data from the database.
     * @since List DB Helper 1.0, april 2019
     */
    public ListDbHelper(Context context) {
        super(context, DATABASE_NAME +".db", null, DATABASE_VERSION);
    }// constructor

    // methods

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder createQuery = new StringBuilder();
        // CREATE TABLE DANCE... wait what!?
        createQuery.append("CREATE TABLE IF NOT EXISTS ")
                .append(InfiniteListDBContract.InfiniteListEntry.TABLE_NAME)
                .append(" (").append(InfiniteListDBContract.InfiniteListEntry._ID)
                .append(" INTEGER PRIMARY KEY DEFAULT 1, ")
                .append(InfiniteListDBContract.InfiniteListEntry.COLUMN_LIST_SIZE)
                .append(" INTEGER CHECK(")
                .append(InfiniteListDBContract.InfiniteListEntry.COLUMN_LIST_SIZE)
                .append(" > 0));");
        db.execSQL(createQuery.toString());
    }//onCreate

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // this is version 1, the very first; gotcha?
    }//onUpgrade

}//ListDbHelper
