package mx.nachintoch.materialdesign.provider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Contract which indicates how to CRUD the Master List's data.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, april 2019
 * @since Material Design Example app 3.0, april 2019
 */
public class InfiniteListDBContract {

    // attributes

    /**
     * This app is the owner and server of the data indicated by this contract, so we use the app's
     * ID as irs URIs' authority.
     * @since Infinite List DB Contract 1.0, april 2019
     */
    public static final String AUTHORITY = "mx.nachintoch.materialdesign";

    /**
     * Base URI to help any user of the data indicated by this contract to easily build a query URI.
     * @since Infinite List DB Contract 1.0, april 2019
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" +AUTHORITY);

    // constructors

    /**
     * Hides the class' constructor, so nobody can instance this utility class.
     * @since Infinite List DB Contract 1.0, april 2019
     */
    private InfiniteListDBContract() {
        // does nothing
    }//constructor

    // nested classes

    /**
     * Defines all needed data to CRUD on the infinite_list table.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
     * @version 1.0, april 2019
     * @since Infinite List DB Contract 1.0, april 2019
     */
    public static class InfiniteListEntry implements BaseColumns {

        // attributes

        /**
         * Path to the Infinite List's data.
         * @since Infinite List DB Contract 1.0, april 2019
         */
        public static final String PATH_INFINITE_LIST = "infinite_list";

        /**
         * URI to this collection's content.
         * @since Infinite List DB Contract 1.0, april 2019
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_INFINITE_LIST).build();

        /**
         * This collection's table name.
         * @since Infinite List DB Contract 1.0, april 2019
         */
        public static final String TABLE_NAME = PATH_INFINITE_LIST;

        /**
         * Name of the list's size column.
         * @since Infinite List DB Contract 1.0, april 2019
         */
        public static final String COLUMN_LIST_SIZE = "list_size";

        /**
         * Index of the ID column.
         * @since Infinite List DB Contract 1.0, april 2019
         */
        public static final byte COLUMN_INDEX_ID = 0;

        /**
         * Index of the list size's column.
         * @since Infinite List DB Contract 1.0, april 2019
         */
        public static final byte COLUMN_INDEX_LIST_SIZE = 1;

        /**
         * All table's columns
         * @since Infinite List DB Contract 1.0, april 2019
         */
        public static final String[] COLUMNS = new String[] {_ID, COLUMN_LIST_SIZE};

        /**
         * General content type of the collection.
         * @since Infinite List DB Contract 1.0, april 2019
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE +"/"
                +AUTHORITY +"/" +PATH_INFINITE_LIST;

        /**
         * Individual content type of each collection's entry.
         * @since Infinite List DB Contract 1.0, april 2019
         */
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE +"/"
                +AUTHORITY +"/" +PATH_INFINITE_LIST;

        // constructor

        /**
         * Hides the constructor so nobody can instance this class.
         * @since Infinite List DB Contract 1.0, april 2019
         */
        private InfiniteListEntry() {
            // nothing to see here, move along...
        }//constructor

        // static methods

        /**
         * Build the URI to point at the entry with the given ID.
         * @param id - The ID of the entry to point at.
         * @return The URI that points to the given entry.
         */
        public static Uri buildListSizeUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }//buildFavouriteUri

    }//InfiniteListEntry Columns

}//InfiniteListDBContract
