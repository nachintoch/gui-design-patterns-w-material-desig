package mx.nachintoch.materialdesign.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Provides the data to recreate the <tt>Master List</tt>.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, april 2019
 * @since Material Design Example App 3.0, april 2019
 */
public class ListProvider extends ContentProvider {

    // attibutes

    /**
     * Helper to communicate with the system's SQLite engine.
     * @since List Provider 1.0, april 2019
     */
    private ListDbHelper dbHelper;

    /**
     * Represents the content type <i>list_size</i>
     * @since List Provider 1.0, april 2019
     */
    public static final byte LIST_SIZE = 100;

    /**
     * Represents the CRUD query for <i>list_size</i> entries using an ID.
     * @since List Provider 1.0, april 2019
     */
    public static final byte LIST_SIZE_WITH_ID = 101;

    /**
     * UriMatcher to match a URI in a CRUD query, so the ListProvider can perform the
     * adequate query on the collection indicated by the PATH in the URI.
     */
    private static final UriMatcher CONTENT_MATCHER;

    // class initializer
    static {
        CONTENT_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        CONTENT_MATCHER.addURI(InfiniteListDBContract.AUTHORITY,
                InfiniteListDBContract.InfiniteListEntry.PATH_INFINITE_LIST, LIST_SIZE);
        CONTENT_MATCHER.addURI(InfiniteListDBContract.AUTHORITY,
                InfiniteListDBContract.InfiniteListEntry.PATH_INFINITE_LIST +"/#",
                LIST_SIZE_WITH_ID);
    }//class initializer

    // methods

    @Override
    public boolean onCreate() {
        dbHelper = new ListDbHelper(getContext());
        return true;
    }//onCreate

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor result;
        switch (CONTENT_MATCHER.match(uri)) {
            case LIST_SIZE :
                result = db.query(InfiniteListDBContract.InfiniteListEntry.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            case LIST_SIZE_WITH_ID :
                result = db.query(InfiniteListDBContract.InfiniteListEntry.TABLE_NAME, projection,
                        InfiniteListDBContract.InfiniteListEntry._ID +" = ?",
                        new String[] {uri.getPathSegments().get(1)}, null, null,
                        null);
                break;
            default :
                throw new UnsupportedOperationException("Invalid URI: " +uri);
        }//performs a query depending on the given URI
        Context context = getContext();
        if(context != null) result.setNotificationUri(context.getContentResolver(), uri);
        return result;
    }//query

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long newId;
        switch (CONTENT_MATCHER.match(uri)) {
            case LIST_SIZE :
                newId = db.insert(InfiniteListDBContract.InfiniteListEntry.TABLE_NAME, null, values);
                break;
            case LIST_SIZE_WITH_ID :
                if(values == null) values = new ContentValues();
                values.put(InfiniteListDBContract.InfiniteListEntry._ID, uri.getPathSegments().get(1));
                newId = db.insert(InfiniteListDBContract.InfiniteListEntry.TABLE_NAME, null, values);
                break;
            default :
                throw new UnsupportedOperationException("Can't insert on " +uri);
        }//inserts on the adequate collection
        if(newId <= 0) throw new SQLException("Couldn't insert");
        return ContentUris.withAppendedId(InfiniteListDBContract.InfiniteListEntry.CONTENT_URI, newId);
    }//insert

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int deletedEntries;
        switch (CONTENT_MATCHER.match(uri)) {
            case LIST_SIZE :
                deletedEntries = db.delete(InfiniteListDBContract.InfiniteListEntry.TABLE_NAME,
                        selection, selectionArgs);
                break;
            case LIST_SIZE_WITH_ID :
                deletedEntries = db.delete(InfiniteListDBContract.InfiniteListEntry.TABLE_NAME,
                        InfiniteListDBContract.InfiniteListEntry._ID +" = ?",
                        new String[] {uri.getPathSegments().get(1)});
                break;
            default :
                throw new UnsupportedOperationException("Can't delete on " +uri);
        }//deletes from the requested collection
        Context context = getContext();
        if(context != null && deletedEntries > 0)
            context.getContentResolver().notifyChange(uri, null);
        return deletedEntries;
    }//delete

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int updatedEntries;
        switch (CONTENT_MATCHER.match(uri)) {
            case LIST_SIZE :
                updatedEntries = db.update(InfiniteListDBContract.InfiniteListEntry.TABLE_NAME,
                        values, selection, selectionArgs);
                break;
            case LIST_SIZE_WITH_ID :
                updatedEntries = db.update(InfiniteListDBContract.InfiniteListEntry.TABLE_NAME,
                        values, InfiniteListDBContract.InfiniteListEntry._ID +" = ?",
                        new String[] {uri.getPathSegments().get(1)});
                break;
            default :
                throw new UnsupportedOperationException("Can't update " +uri);
        }//updates a given
        Context context = getContext();
        if(context != null && updatedEntries > 0) context.getContentResolver().notifyChange(uri, null);
        return updatedEntries;
    }//update

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (CONTENT_MATCHER.match(uri)) {
            case LIST_SIZE :
            case LIST_SIZE_WITH_ID :
                return InfiniteListDBContract.InfiniteListEntry.CONTENT_TYPE;
            default :
                throw new UnsupportedOperationException("Unknown type for " +uri);
        }//returns the content type for the given URI
    }//getType

}//List Provider
